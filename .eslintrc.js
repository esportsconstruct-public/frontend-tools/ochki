"use strict";

module.exports = {
    extends: ["@esportsconstruct-public", "prettier"],
    parserOptions: {
        project: "./tsconfig.json"
    },
    rules: {
        "no-console": "off"
    },
    overrides: []
};
