import fs from "fs";
import { DirsMaster } from "./core/dirs-master";

// DO NOT DELETE THIS FILE
// This file is used by build system to build a clean npm package with the compiled js files in the root of the package.
// It will not be included in the npm package.

function main() {
    const allowedFieldsFromPackageJson = [
        "name",
        "version",
        "description",
        "main",
        "types",
        "repository",
        "keywords",
        "author",
        "license",
        "bugs",
        "dependencies",
        "publishConfig",
        "bin"
    ];

    const source = fs.readFileSync(__dirname + "/../package.json").toString("utf-8");
    const sourceObj = JSON.parse(source);

    Object.keys(sourceObj).forEach((key) => {
        if (!allowedFieldsFromPackageJson.includes(key)) {
            sourceObj[key] = {};
        }
    });

    sourceObj.main = sourceObj.main.replace("dist/", "");
    sourceObj.types = sourceObj.types.replace("dist/", "");

    fs.writeFileSync(
        __dirname + "/package.json",
        Buffer.from(JSON.stringify(sourceObj, null, 2), "utf-8")
    );

    fs.copyFileSync(__dirname + "/../.npmignore", __dirname + "/.npmignore");
    fs.copyFileSync(__dirname + "/../.npmrc", __dirname + "/.npmrc");

    DirsMaster.copyFolderRecursiveSync(__dirname + "/../src/assets", "./dist");
}

main();
