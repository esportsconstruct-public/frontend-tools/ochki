import { HeadersConfig, TesterConfig } from "./interfaces/tester-config";
import { ScreenshotMaster } from "./screenshot-master";
import { ReportGenerator } from "./report-generator";
import { DirsMaster } from "./dirs-master";
import { FolderStructure } from "./interfaces/folder-structure";

export class Tester {
    private screenshotMaster: ScreenshotMaster;
    private readonly report: ReportGenerator;
    private readonly dirsMaster: DirsMaster;
    private readonly config: TesterConfig;
    private readonly headers?: HeadersConfig;
    private folderStructure: FolderStructure;

    public constructor(config: TesterConfig, headers?: HeadersConfig) {
        this.config = config;
        this.report = new ReportGenerator(config);
        this.dirsMaster = new DirsMaster(config);
        this.headers = headers;
    }

    public async test(): Promise<void> {
        console.time("generator");
        process.removeAllListeners();
        process.setMaxListeners(1000);

        this.dirsMaster.clearReport();
        console.info(`Removed report directory`);

        this.dirsMaster.generateReportFolders();
        console.info(`Generated report directory`);

        this.folderStructure = new FolderStructure(this.config);
        console.info(`Generated image directory`);

        this.screenshotMaster = new ScreenshotMaster(
            this.config,
            this.folderStructure,
            this.headers
        );

        console.info(`Screenshots creating...`);
        await this.screenshotMaster.generate();

        console.info(`Generating report...`);
        await this.report.generate(this.folderStructure);

        console.timeEnd("generator");
    }
}
