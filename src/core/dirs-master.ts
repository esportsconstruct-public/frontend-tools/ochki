import fs from "fs";
import Path from "path";
import { TesterConfig } from "./interfaces/tester-config";
import path from "path";

export class DirsMaster {
    private readonly config: TesterConfig;

    public constructor(config: TesterConfig) {
        this.config = config;
    }

    public clearReport(): void {
        this.removeDirectory(`./${this.config.dirs.base}/${this.config.dirs.report}`);
    }

    public generateReportFolders(): void {
        let dir = `./${this.config.dirs.base}`;

        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }

        dir += `/${this.config.dirs.report}`;

        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }

        dir += `/${this.config.dirs.images}`;

        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }
    }

    private readonly removeDirectory = (path: string): void => {
        if (fs.existsSync(path)) {
            fs.readdirSync(path).forEach((file) => {
                const curPath = Path.join(path, file);
                if (fs.lstatSync(curPath).isDirectory()) {
                    // recurse
                    this.removeDirectory(curPath);
                } else {
                    // delete file
                    fs.unlinkSync(curPath);
                }
            });
            fs.rmdirSync(path);
        }
    };

    public static getFilePath(
        config: TesterConfig,
        theme: string,
        pageName: string,
        viewportName: string,
        fileName: string
    ): string {
        return `./${config.dirs.base}/${config.dirs.report}/${
            config.dirs.images
        }/${DirsMaster.clearFileName(theme)}/${DirsMaster.clearFileName(
            pageName
        )}/${DirsMaster.clearFileName(viewportName)}/${fileName}.png`;
    }

    public static writeTextFile(path: string, text: string): void {
        fs.writeFile(path, text, (e) => {
            if (e) throw e;
            console.log(`${path} was created successfully`);
        });
    }

    public static createFolders(base: string, names: string[]): string {
        let dir = base;
        names.forEach((name) => {
            dir += `/${name}`;

            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }
        });

        return dir;
    }

    public static copyFolderRecursiveSync(source: string, target: string): void {
        let files = [];

        //check if folder needs to be created or integrated
        const targetFolder = path.join(target, path.basename(source));
        if (!fs.existsSync(targetFolder)) {
            fs.mkdirSync(targetFolder);
        }

        //copy
        if (fs.lstatSync(source).isDirectory()) {
            files = fs.readdirSync(source);
            files.forEach(function (file) {
                var curSource = path.join(source, file);
                if (fs.lstatSync(curSource).isDirectory()) {
                    DirsMaster.copyFolderRecursiveSync(curSource, targetFolder);
                } else {
                    DirsMaster.copyFileSync(curSource, targetFolder);
                }
            });
        }
    }

    public static clearFileName(fileName: string): string {
        return this.trimByChar(this.trimByChar(fileName, "."), "#")
            .replace(/ /gu, "-")
            .replace(/\./gu, "-")
            .replace(/#/gu, "-")
            .toLowerCase();
    }

    public static exist(path: string): boolean {
        return fs.existsSync(path);
    }

    private static trimByChar(string: string, character: string): string {
        const re = new RegExp("^[" + character + "]+|[" + character + "]+$", "gu");
        return string.replace(re, "");
    }

    private static copyFileSync(source: string, target: string): void {
        var targetFile = target;

        //if target is a directory a new file with the same name will be created
        if (fs.existsSync(target)) {
            if (fs.lstatSync(target).isDirectory()) {
                targetFile = path.join(target, path.basename(source));
            }
        }

        fs.writeFileSync(targetFile, fs.readFileSync(source));
    }
}
