export interface CompareImagesData {
    isSameDimensions: boolean;
    dimensionDifference: {
        width: number;
        height: number;
    };
    rawMisMatchPercentage: number;
    misMatchPercentage: number;
    diffBounds: {
        top: number;
        left: number;
        bottom: number;
        right: number;
    };
    analysisTime: number;
    getImageDataUrl: () => string;
    getBuffer: () => Buffer;
}
