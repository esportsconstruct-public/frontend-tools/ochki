import { Selector, SelectorAction, TesterConfig } from "./tester-config";
import { DirsMaster } from "../dirs-master";

export interface ViewportFromFolderStructure {
    selectors?: Selector[];
    actions?: SelectorAction[];
    viewportHeight?: number;
}

export interface FolderStructureData {
    [key: string]: {
        // page
        [key: string]: {
            // viewport: filenames[]
            [key: string]: ViewportFromFolderStructure;
        };
    };
}

export class FolderStructure {
    // theme
    public data: FolderStructureData = {};
    public count = 0;

    private readonly config: TesterConfig;

    public constructor(config: TesterConfig) {
        this.config = config;

        this.config.themes.forEach((theme) => {
            this.config.pages.forEach((pageConfig) => {
                this.config.viewports.forEach((viewportConfig) => {
                    pageConfig.selectors.forEach((selectorConfig) => {
                        selectorConfig.viewport.forEach((viewport) => {
                            if (viewportConfig.name === viewport) {
                                if (selectorConfig.selectors) {
                                    selectorConfig.selectors.forEach((selector) => {
                                        this.updateFolderStructure(
                                            theme.toLowerCase(),
                                            pageConfig.name.toLowerCase(),

                                            viewportConfig.name.toLowerCase(),
                                            selector,
                                            undefined,
                                            selectorConfig.viewportHeight
                                        );
                                    });
                                }
                                if (selectorConfig.actions) {
                                    selectorConfig.actions.forEach((action) => {
                                        this.updateFolderStructure(
                                            theme.toLowerCase(),
                                            pageConfig.name.toLowerCase(),
                                            viewportConfig.name.toLowerCase(),
                                            undefined,
                                            action,
                                            selectorConfig.viewportHeight
                                        );
                                    });
                                }
                            }
                        });
                    });
                });
            });
        });

        this.recheckCount();
    }

    private updateFolderStructure(
        theme: string,
        pageName: string,
        viewportName: string,
        selector?: Selector,
        action?: SelectorAction,
        viewportHeight?: number
    ): void {
        DirsMaster.createFolders(
            `./${this.config.dirs.base}/${this.config.dirs.report}/${this.config.dirs.images}`,
            [
                DirsMaster.clearFileName(theme),
                DirsMaster.clearFileName(pageName),
                DirsMaster.clearFileName(viewportName)
            ]
        );

        if (!Object.keys(this.data).find((key) => key === theme)) {
            this.data[theme] = {};
        }

        if (!Object.keys(this.data[theme]).find((key) => key === pageName)) {
            this.data[theme][pageName] = {};
        }

        if (!Object.keys(this.data[theme][pageName]).find((key) => key === viewportName)) {
            this.data[theme][pageName][viewportName] = {};
        }

        if (selector) {
            const selectors = this.data[theme][pageName][viewportName].selectors;

            if (!selectors) {
                this.data[theme][pageName][viewportName].selectors = [selector];
            } else {
                this.data[theme][pageName][viewportName].selectors = [...selectors, selector];
            }
        }

        if (action) {
            const actions = this.data[theme][pageName][viewportName].actions;
            if (!actions) {
                this.data[theme][pageName][viewportName].actions = [action];
            } else {
                this.data[theme][pageName][viewportName].actions = [...actions, action];
            }
        }

        if (viewportHeight) {
            this.data[theme][pageName][viewportName].viewportHeight = viewportHeight;
        }
    }

    private recheckCount(): void {
        Object.keys(this.data).forEach((theme) => {
            Object.keys(this.data[theme]).forEach((page) => {
                Object.keys(this.data[theme][page]).forEach((viewport) => {
                    this.count += this.data[theme][page][viewport].selectors?.length ?? 0;
                    this.count +=
                        this.data[theme][page][viewport].actions?.filter(
                            (action) => action.type === "screenshot"
                        ).length ?? 0;
                });
            });
        });
    }
}
