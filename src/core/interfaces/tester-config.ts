export interface TesterConfig {
    title?: string;
    dirs: {
        base: string;
        report: string;
        images: string;
    };
    testHost: string;
    refHost: string;
    themes: string[];
    viewports: Array<{
        name: string;
        width: number;
        height: number;
        userAgent: string;
    }>;
    beforeEach?: {
        endpoint: string;
        actions: SelectorAction[];
    };
    pages: ScreenshotMasterConfigPage[];
    misMatchPercentage: number;
    values?: {
        [key: string]: string;
    };
    delay?: number;
}

export interface ScreenshotMasterConfigPage {
    name: string;
    endpoint: string;
    queryParams?: {
        [key: string]: string;
    };
    selectors: Array<{
        viewport: string[];
        selectors?: Selector[];
        actions?: SelectorAction[];
        viewportHeight?: number;
    }>;
}

export interface Selector {
    name: string;
    value: string;
    index?: number;
    delay?: number;
    wait?: string;
    waitHidden?: string;
    margin?: {
        top?: number;
        left?: number;
        right?: number;
        bottom?: number;
    };
    // actions?: SelectorAction[];
}

export interface SelectorAction {
    name?: string;
    selector: string;
    type: "click" | "hover" | "screenshot" | "input";
    wait?: string;
    waitHidden?: string;
    waitAfter?: string;
    // you can use --key where key is key from values object in root
    inputValue?: string;
    delay?: number;
    index?: number;
}

export interface HeadersConfig {
    cfAccessClientId?: string;
    cfAccessClientSecret?: string;
}
