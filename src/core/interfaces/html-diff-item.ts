export interface HtmlDiffItem {
    theme: string;
    page: string;
    viewport: string;
    selectorName: string;
    featureImg: string;
    originalImg: string;
    diffImg: string;
}
