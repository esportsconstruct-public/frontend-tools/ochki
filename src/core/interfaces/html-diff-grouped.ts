export interface HtmlDiffGrouped {
    page: string;
    items: Array<{
        component: string;
        items: Array<{
            theme: string;
            items: HtmlDiffGroupedItem[];
        }>;
    }>;
}

export interface HtmlDiffGroupedItem {
    viewport: string;
    featureImg: string;
    originalImg: string;
    diffImg: string;
}
