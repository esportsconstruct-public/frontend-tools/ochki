import { HtmlDiffGroupedItem } from "../interfaces/html-diff-grouped";

export const diffItemHtml = (item: HtmlDiffGroupedItem): string => `
  <div class="item">
    <div class="viewport-name">${item.viewport}</div>
   <div class="item-columns">
    <div class="item-column">
      <img class="image ${
          item.originalImg === "./assets/placeholder.png" ? "placeholder" : ""
      }" src="${item.originalImg}" onclick="clickToImage(this)" alt="image">
    </div>
    <div class="item-column">
      <img class="image ${
          item.featureImg === "./assets/placeholder.png" ? "placeholder" : ""
      }" src="${item.featureImg}" onclick="clickToImage(this)" alt="image">
    </div>
    <div class="item-column">
     <img class="image ${item.diffImg === "./assets/placeholder.png" ? "placeholder" : ""}" src="${
    item.diffImg
}" onclick="clickToImage(this)" alt="image">
    </div>
  </div>
</div>
`;
