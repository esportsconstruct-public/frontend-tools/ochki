export const pageNameHtml = (name: string): string => `
  <div class="page-name">${name}</div>
`;
