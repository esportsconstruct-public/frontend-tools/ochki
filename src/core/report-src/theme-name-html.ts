export const themeNameHtml = (name: string): string => `
  <div class="theme-name">${name} theme</div>
`;
