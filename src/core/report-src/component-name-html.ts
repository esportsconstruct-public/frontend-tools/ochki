export const componentNameHtml = (name: string): string => `
  <div class="component-name">${name}</div>
`;
