import { diffItemHtml } from "./item-html";
import { HtmlDiffGrouped } from "../interfaces/html-diff-grouped";
import { pageNameHtml } from "./page-name-html";
import { componentNameHtml } from "./component-name-html";
import { themeNameHtml } from "./theme-name-html";

export const indexHtml = (data: {
    title: string;
    refHost: string;
    testHost: string;
    successItems: HtmlDiffGrouped[];
    failedItems: HtmlDiffGrouped[];
    successCount: number;
    failedCount: number;
}): string => `
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>${data.title}</title>
        <link rel="stylesheet" type="text/css" href="./assets/main.css">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.6.2/viewer.min.css" rel="stylesheet">
    </head>
    <body>
      <div class="header">
          <div class="header-content">
              <div class="header-actions">
              <div class="header-btn failed" id="header-btn-failed" onclick="changeActiveTab('failed')">Failed ${
                  data.failedCount
              }
              </div>
                <div class="header-btn success" id="header-btn-success" onclick="changeActiveTab('success')">Success ${
                    data.successCount
                }</div>
              </div>
              <div class="header-title">${data.title}</div>
              <div class="header-actions">

              </div>
          </div>
      </div>
      <div class="main-container">
          <div class="main-content">
            <div id="success-body" class="diff-body">

                  <div class="item-columns-header">
                    <div class="item-column">
                      <div class="item-column-label">Original (${data.refHost})</div>
                    </div>
                    <div class="item-column">
                      <div class="item-column-label">Feature (${data.testHost})</div>
                    </div>
                    <div class="item-column">
                      <div class="item-column-label">Diff</div>
                    </div>
                  </div>

                  ${data.successItems
                      .map((pages) => {
                          return (
                              pageNameHtml(pages.page) +
                              pages.items
                                  .map((page) => {
                                      return (
                                          componentNameHtml(page.component) +
                                          page.items
                                              .map((component) => {
                                                  return (
                                                      themeNameHtml(component.theme) +
                                                      component.items
                                                          .map((theme) => {
                                                              return diffItemHtml(theme);
                                                          })
                                                          .join("\n")
                                                  );
                                              })
                                              .join("\n")
                                      );
                                  })
                                  .join("\n")
                          );
                      })
                      .join("\n")}
             </div>
            <div id="failed-body" class="diff-body">
                  <div class="item-columns-header">
                    <div class="item-column">
                      <div class="item-column-label">Original (${data.refHost})</div>
                    </div>
                    <div class="item-column">
                      <div class="item-column-label">Feature (${data.testHost})</div>
                    </div>
                    <div class="item-column">
                      <div class="item-column-label">Diff</div>
                    </div>
                  </div>
                   ${data.failedItems
                       .map((pages) => {
                           return (
                               pageNameHtml(pages.page) +
                               pages.items
                                   .map((page) => {
                                       return (
                                           componentNameHtml(page.component) +
                                           page.items
                                               .map((component) => {
                                                   return (
                                                       themeNameHtml(component.theme) +
                                                       component.items
                                                           .map((theme) => {
                                                               return diffItemHtml(theme);
                                                           })
                                                           .join("\n")
                                                   );
                                               })
                                               .join("\n")
                                       );
                                   })
                                   .join("\n")
                           );
                       })
                       .join("\n")}
              </div>
          </div>
      </div>
    </body>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.6.2/viewer.min.js"></script>
    <script>
       var activeTab = 'failed';
       recheckActiveTab();

       function recheckActiveTab() {
           if (activeTab === 'failed') {
               document.getElementById('failed-body').classList.remove('hidden');
               document.getElementById('success-body').classList.add('hidden');

               document.getElementById('header-btn-failed').classList.add('active');
               document.getElementById('header-btn-success').classList.remove('active');
           }
           if (activeTab === 'success') {
               document.getElementById('failed-body').classList.add('hidden');
               document.getElementById('success-body').classList.remove('hidden');

               document.getElementById('header-btn-failed').classList.remove('active');
               document.getElementById('header-btn-success').classList.add('active');
           }
       }

       function changeActiveTab(newTabName) {
          activeTab = newTabName;
          recheckActiveTab();
       }

       function clickToImage(element) {
           const viewer = new Viewer(element, {
              viewed() {
                viewer.zoomTo(1);
              },
              navbar: false,
              toolbar: false
            });
            viewer.show();
       }

    </script>
</html>
`;
