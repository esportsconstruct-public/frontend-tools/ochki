import { HeadersConfig, Selector, SelectorAction, TesterConfig } from "./interfaces/tester-config";
import { ElementHandle, EmulateOptions, Page, Response } from "puppeteer";
import { Cluster } from "puppeteer-cluster";
import { DirsMaster } from "./dirs-master";
import { FolderStructure } from "./interfaces/folder-structure";

export class ScreenshotMaster {
    private readonly config: TesterConfig;
    private readonly folderStructure: FolderStructure;
    private readonly headers?: HeadersConfig;

    public constructor(
        config: TesterConfig,
        folderStructure: FolderStructure,
        headers?: HeadersConfig
    ) {
        this.config = config;
        this.folderStructure = folderStructure;
        this.headers = headers;
    }

    public async generate(): Promise<void> {
        console.info(`[0/${Object.keys(this.folderStructure.data).length}] Screenshot creating...`);

        for await (const { index, theme } of Object.keys(
            this.folderStructure.data
        ).map((theme, index) => ({ index, theme }))) {
            console.time(theme);

            const createImagesPromises: Array<Promise<void>> = [];
            Object.keys(this.folderStructure.data[theme]).forEach((pageName) => {
                createImagesPromises.push(this.createImages(theme, pageName));
            });
            await Promise.all(createImagesPromises);

            console.info(
                `[${index + 1}/${
                    Object.keys(this.folderStructure.data).length
                }] ${theme} theme is done`
            );
            console.timeEnd(theme);
        }
    }

    private async createImages(theme: string, pageName: string): Promise<void> {
        let cluster: Cluster<
            {
                url: string;
                isOriginal: boolean;
                emulateOptions: EmulateOptions;
                viewportName: string;
            },
            void
        >;

        try {
            // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
            cluster = await Cluster.launch({
                puppeteerOptions: {
                    headless: true,
                    args: ["--no-sandbox", "--disable-setuid-sandbox"]
                },
                concurrency: Cluster.CONCURRENCY_CONTEXT,
                sameDomainDelay: 2000,
                workerCreationDelay: 2000,
                maxConcurrency: 2,
                timeout: 90000
            });
        } catch (e) {
            console.error("\x1b[31m%s\x1b[0m", `Can't create cluster`, e);
            throw e;
        }

        await cluster.task(async ({ page, data }) => {
            if (this.config.beforeEach) {
                await this.doBeforeEach(page, data.url, data.isOriginal);
            }

            await this.goto(page, data.url, data.isOriginal);

            let emulateOptions: EmulateOptions;
            const viewportHeight = this.folderStructure.data[theme][pageName][data.viewportName]
                .viewportHeight;
            if (viewportHeight) {
                emulateOptions = {
                    ...data.emulateOptions,
                    viewport: {
                        ...data.emulateOptions.viewport,
                        height: viewportHeight
                    }
                };
            } else {
                emulateOptions = data.emulateOptions;
            }

            await page.emulate(emulateOptions);
            // ToDo remove unknown after updates in @types/puppeteer
            await ((page as unknown) as {
                waitForTimeout: (number: number) => Promise<void>;
            }).waitForTimeout(3000);

            const selectors = this.folderStructure.data[theme][pageName][data.viewportName]
                .selectors;
            if (selectors?.length) {
                const getAllSelectors: Array<Promise<void>> = [];
                selectors.forEach((selector) => {
                    getAllSelectors.push(
                        this.createImage(
                            page,
                            pageName,
                            selector,
                            theme,
                            data.viewportName,
                            data.isOriginal
                        )
                    );
                });

                await page.reload({ waitUntil: ["networkidle0", "domcontentloaded"] });
                await Promise.all(getAllSelectors);
            }

            if (this.folderStructure.data[theme][pageName][data.viewportName].actions?.length) {
                await page.reload({ waitUntil: ["networkidle0", "domcontentloaded"] });

                if (this.config.delay) {
                    await ((page as unknown) as {
                        waitForTimeout: (number: number) => Promise<void>;
                    }).waitForTimeout(this.config.delay);
                }

                const actions = this.folderStructure.data[theme][pageName][data.viewportName]
                    .actions;
                if (actions?.length) {
                    for (const { index, action } of actions.map((action, index) => ({
                        index,
                        action
                    }))) {
                        if (action.type === "screenshot") {
                            // eslint-disable-next-line no-await-in-loop
                            await this.createImage(
                                page,
                                pageName,
                                {
                                    name: action.name ?? "Action",
                                    value: action.selector,
                                    delay: action.delay,
                                    index: action.index,
                                    wait: action.wait,
                                    waitHidden: action.waitHidden
                                },
                                theme,
                                data.viewportName,
                                data.isOriginal
                            );
                        } else {
                            try {
                                // eslint-disable-next-line no-await-in-loop
                                await this.doActions(page, action);
                            } catch (err) {
                                console.error(
                                    "\x1b[31m%s\x1b[0m",
                                    `[${
                                        data.isOriginal ? "Original" : "Feature"
                                    }] Can't do action on ${pageName} for ${theme} theme for ${
                                        data.viewportName
                                    } index ${index}`,
                                    action
                                );
                                break;
                            }
                        }
                    }
                }
            }
        });

        for await (const viewportName of Object.keys(this.folderStructure.data[theme][pageName])) {
            const viewportConfig = this.config.viewports.find(
                (viewport) => viewport.name === viewportName
            );
            const pageConfig = this.config.pages.find(
                (page) => page.name.toLowerCase() === pageName
            );

            if (viewportConfig && pageConfig) {
                const emulateOptions: EmulateOptions = {
                    viewport: { width: viewportConfig.width, height: viewportConfig.height },
                    userAgent: viewportConfig.userAgent
                };

                const endpoint = pageConfig.endpoint;
                // Add some pages to queue
                const queryParamsString = pageConfig.queryParams
                    ? `&${Object.keys(pageConfig.queryParams)
                          .map((key) =>
                              pageConfig.queryParams ? `${key}=${pageConfig.queryParams[key]}` : ""
                          )
                          .join("&")}`
                    : "";

                await cluster.queue({
                    url: `${this.config.testHost}${endpoint}?theme=${theme}${queryParamsString}`,
                    isOriginal: false,
                    emulateOptions,
                    viewportName
                });
                await cluster.queue({
                    url: `${this.config.refHost}${endpoint}?theme=${theme}${queryParamsString}`,
                    isOriginal: true,
                    emulateOptions,
                    viewportName
                });
            }
        }

        // Shutdown after everything is done
        await cluster.idle();
        await cluster.close();
    }

    private async doBeforeEach(page: Page, hostUrl: string, isOriginal: boolean): Promise<void> {
        if (this.config.beforeEach) {
            await this.goto(page, `${hostUrl}${this.config.beforeEach.endpoint}`, isOriginal);
            for await (const { index, action } of this.config.beforeEach.actions.map(
                (action, index) => ({
                    index,
                    action
                })
            )) {
                try {
                    await this.doActions(page, action);
                } catch (err) {
                    console.error(
                        "\x1b[31m%s\x1b[0m",
                        `[BeforeEach] Can't do action for index ${index} for ${hostUrl}`,
                        action
                    );
                    break;
                }
            }
        }
    }

    private async goto(
        page: Page,
        url: string,
        needAddAuthHeaders = false
    ): Promise<Response | null> {
        if (needAddAuthHeaders && this.headers) {
            // enable request interception
            await page.setRequestInterception(true);
            // add header for the navigation requests
            page.on("request", (request) => {
                // Add a new header for navigation request.
                const headers = request.headers();

                if (this.headers?.cfAccessClientId && this.headers.cfAccessClientSecret) {
                    headers["CF-Access-Client-Id"] = this.headers.cfAccessClientId;
                    headers["CF-Access-Client-Secret"] = this.headers.cfAccessClientSecret;
                }
                // eslint-disable-next-line @typescript-eslint/no-floating-promises
                request.continue({ headers });
            });
        }

        return page.goto(url, {
            waitUntil: ["networkidle0", "domcontentloaded"]
        });
    }

    private async doActions(page: Page, action: SelectorAction): Promise<void> {
        await page.waitForSelector(action.selector, { visible: true });

        let actionElements = await page.$$(action.selector);
        // ToDo remove unknown after updates in @types/puppeteer
        await ((page as unknown) as {
            waitForTimeout: (number: number) => Promise<void>;
        }).waitForTimeout(50);

        if (action.type === "click") {
            await actionElements[action.index ?? 0].click();
        }

        if (action.type === "hover") {
            await actionElements[action.index ?? 0].hover();
        }

        if (action.type === "input") {
            let text = action.inputValue ?? "";
            if (text.startsWith("--value-") && this.config.values?.hasOwnProperty(text)) {
                text = this.config.values[text];
            }
            await page.keyboard.type(text);
        }

        if (action.waitAfter) {
            await page.waitForSelector(action.waitAfter, { visible: true });
        }

        // ToDo remove unknown after updates in @types/puppeteer
        await ((page as unknown) as {
            waitForTimeout: (number: number) => Promise<void>;
        }).waitForTimeout(action.delay ? action.delay : 50);
    }

    private async createImage(
        page: Page,
        pageName: string,
        selector: Selector,
        theme: string,
        viewportName: string,
        isOriginal: boolean
    ): Promise<void> {
        try {
            if (selector.wait) {
                await page.waitForSelector(selector.wait, { visible: true });
            }
            if (selector.waitHidden) {
                try {
                    await page.waitForSelector(selector.waitHidden, { hidden: true });
                } catch (err) {
                    console.log("catch in waitHidden");
                }
            }

            try {
                await page.waitForSelector(selector.value, { visible: true });
            } catch (err) {
                // await page.screenshot({ path: "1.png"})
                console.log("catch in wait for selector");
            }

            // ToDo remove unknown after updates in @types/puppeteer
            await ((page as unknown) as {
                waitForTimeout: (number: number) => Promise<void>;
            }).waitForTimeout(selector.delay ? selector.delay : 100);

            const elementsHandle: ElementHandle[] = await this.getElements(page, selector.value);

            let fileName = DirsMaster.clearFileName(selector.name);
            if (isOriginal) {
                fileName += `.original`;
            }
            const elementHandle = elementsHandle[selector.index ?? 0];

            /**
             *  hack for margins
             *  @see https://github.com/puppeteer/puppeteer/issues/1010#issuecomment-335974797
             */

            const clip = Object.assign({}, await elementHandle.boundingBox());

            if (selector.margin?.bottom) {
                clip.height += selector.margin.bottom;
            }
            if (selector.margin?.top) {
                clip.y -= selector.margin.top;
                clip.height += selector.margin.top;
            }
            if (selector.margin?.left) {
                clip.x -= selector.margin.left;
                clip.width += selector.margin.left;
            }
            if (selector.margin?.right) {
                clip.width += selector.margin.right;
            }

            await elementHandle.screenshot({
                path: DirsMaster.getFilePath(this.config, theme, pageName, viewportName, fileName),
                clip
            });
        } catch (e) {
            console.error(
                "\x1b[31m%s\x1b[0m",
                `[${isOriginal ? "Original" : "Feature"}] Can't get ${
                    selector.name
                } on ${pageName} for ${theme} theme for ${viewportName}`
            );
        }
    }

    private async getElements(page: Page, selector: string, attempt = 1): Promise<ElementHandle[]> {
        try {
            return await page.$$(selector);
        } catch (e) {
            if (attempt <= 3) {
                console.error("\x1b[31m%s\x1b[0m", `[Attempt ${attempt}/3] Can't get ${selector}`);
                await this.getElements(page, selector, attempt + 1);
            } else {
                throw e;
            }
            return [];
        }
    }
}
