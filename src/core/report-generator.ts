import { DirsMaster } from "./dirs-master";
import { Selector, TesterConfig } from "./interfaces/tester-config";
import { indexHtml } from "./report-src";
import { FolderStructure } from "./interfaces/folder-structure";
import compareImages from "resemblejs/compareImages";
import fs from "fs";
import { CompareImagesData } from "./interfaces/compare-images-data";
import { HtmlDiffItem } from "./interfaces/html-diff-item";
import { HtmlDiffGrouped } from "./interfaces/html-diff-grouped";
import path from "path";
import { ComparisonOptions } from "resemblejs";

export class ReportGenerator {
    private readonly config: TesterConfig;
    private folderStructure: FolderStructure;
    private readonly options: ComparisonOptions = {
        output: {
            errorColor: {
                red: 255,
                green: 0,
                blue: 255
            },
            errorType: "diffOnly",
            transparency: 1,
            largeImageThreshold: 2000,
            useCrossOrigin: false
        },
        scaleToSameSize: true,
        ignore: "antialiasing"
    };

    public constructor(config: TesterConfig) {
        this.config = config;
    }

    public async generate(folderStructure: FolderStructure): Promise<void> {
        this.folderStructure = folderStructure;

        console.info(`Compare images...`);
        const diff = await this.getDiff();

        console.info(`Generating index...`);
        this.generateIndexHtml(
            this.groupDiffItemsByPage(diff.successItems),
            this.groupDiffItemsByPage(diff.failedItems),
            diff.successItems.length,
            diff.failedItems.length
        );

        console.info(`Coping assets...`);
        this.copyAssets(this.config.dirs.base, this.config.dirs.report);
    }

    private groupDiffItemsByPage(htmlDiffItems: HtmlDiffItem[]): HtmlDiffGrouped[] {
        const result: HtmlDiffGrouped[] = [];

        htmlDiffItems.forEach((diffItem) => {
            let foundPage = result.find((item) => item.page === diffItem.page);
            if (!foundPage) {
                foundPage = {
                    page: diffItem.page,
                    items: []
                };
                result.push(foundPage);
            }

            let foundComponent = foundPage.items.find(
                (item) => item.component === diffItem.selectorName
            );
            if (!foundComponent) {
                foundComponent = {
                    component: diffItem.selectorName,
                    items: []
                };
                foundPage.items.push(foundComponent);
            }

            let foundTheme = foundComponent.items.find((item) => item.theme === diffItem.theme);
            if (!foundTheme) {
                foundTheme = {
                    theme: diffItem.theme,
                    items: []
                };
                foundComponent.items.push(foundTheme);
            }

            foundTheme.items.push({
                viewport: diffItem.viewport,
                featureImg: diffItem.featureImg,
                originalImg: diffItem.originalImg,
                diffImg: diffItem.diffImg
            });
        });

        return result;
    }

    private async getDiff(): Promise<{
        successItems: HtmlDiffItem[];
        failedItems: HtmlDiffItem[];
    }> {
        let successItems: HtmlDiffItem[] = [];
        let failedItems: HtmlDiffItem[] = [];
        for await (const theme of Object.keys(this.folderStructure.data)) {
            for await (const page of Object.keys(this.folderStructure.data[theme])) {
                for await (const viewport of Object.keys(this.folderStructure.data[theme][page])) {
                    const selectors = this.folderStructure.data[theme][page][viewport].selectors;
                    if (selectors?.length) {
                        for await (const selector of selectors) {
                            const result = await this.getDiffOne(
                                DirsMaster.clearFileName(theme),
                                DirsMaster.clearFileName(page),
                                DirsMaster.clearFileName(viewport),
                                selector
                            );
                            successItems = [...successItems, ...result.successItems];
                            failedItems = [...failedItems, ...result.failedItems];
                        }
                    }

                    const actions = this.folderStructure.data[theme][page][viewport].actions;
                    if (actions?.length) {
                        for await (const action of actions) {
                            if (action.type === "screenshot") {
                                const result = await this.getDiffOne(
                                    DirsMaster.clearFileName(theme),
                                    DirsMaster.clearFileName(page),
                                    DirsMaster.clearFileName(viewport),
                                    {
                                        name: action.name ?? "Action",
                                        value: action.selector,
                                        index: action.index
                                    }
                                );
                                successItems = [...successItems, ...result.successItems];
                                failedItems = [...failedItems, ...result.failedItems];
                            }
                        }
                    }
                }
            }
        }

        return { successItems, failedItems };
    }

    private async getDiffOne(
        theme: string,
        page: string,
        viewport: string,
        selector: Selector
    ): Promise<{
        successItems: HtmlDiffItem[];
        failedItems: HtmlDiffItem[];
    }> {
        const successItems: HtmlDiffItem[] = [];
        const failedItems: HtmlDiffItem[] = [];

        const file1Path = DirsMaster.getFilePath(
            this.config,
            theme,
            page,
            viewport,
            DirsMaster.clearFileName(selector.name)
        );
        const file2Path = DirsMaster.getFilePath(
            this.config,
            theme,
            page,
            viewport,
            `${DirsMaster.clearFileName(selector.name)}.original`
        );
        const outputFilePath = DirsMaster.getFilePath(
            this.config,
            theme,
            page,
            viewport,
            `${DirsMaster.clearFileName(selector.name)}.output`
        );

        const item: HtmlDiffItem = {
            theme,
            page,
            viewport,
            selectorName: selector.name,
            featureImg: "",
            originalImg: "",
            diffImg: ""
        };

        let file1Buffer: Buffer | undefined;
        try {
            file1Buffer = await fs.promises.readFile(file1Path);
            item.featureImg = `./${
                this.config.dirs.images
            }/${theme}/${page}/${viewport}/${DirsMaster.clearFileName(selector.name)}.png`;
        } catch (err) {
            item.featureImg = `./assets/placeholder.png`;
        }

        let file2Buffer: Buffer | undefined;
        try {
            file2Buffer = await fs.promises.readFile(file2Path);
            item.originalImg = `./${
                this.config.dirs.images
            }/${theme}/${page}/${viewport}/${DirsMaster.clearFileName(selector.name)}.original.png`;
        } catch (err) {
            item.originalImg = `./assets/placeholder.png`;
        }

        if (file1Buffer && file2Buffer) {
            try {
                const result = await this.compareImages(file2Buffer, file1Buffer, outputFilePath);
                item.diffImg = `./${
                    this.config.dirs.images
                }/${theme}/${page}/${viewport}/${DirsMaster.clearFileName(
                    selector.name
                )}.output.png`;

                if (result) {
                    successItems.push(item);
                } else {
                    failedItems.push(item);
                }
            } catch (e) {
                console.error("\x1b[31m%s\x1b[0m", `Can't compare images`, file1Path);
                item.diffImg = `./assets/placeholder.png`;
                failedItems.push(item);
            }
        } else {
            item.diffImg = `./assets/placeholder.png`;
            failedItems.push(item);
        }

        return { successItems, failedItems };
    }

    private async compareImages(file1: Buffer, file2: Buffer, output: string): Promise<boolean> {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-call
        const data = (await compareImages(file1, file2, this.options)) as CompareImagesData;
        await fs.promises.writeFile(output, data.getBuffer());
        return this.config.misMatchPercentage > data.rawMisMatchPercentage;
    }

    private generateIndexHtml(
        successItems: HtmlDiffGrouped[],
        failedItems: HtmlDiffGrouped[],
        successCount: number,
        failedCount: number
    ): void {
        DirsMaster.writeTextFile(
            `./${this.config.dirs.base}/${this.config.dirs.report}/index.html`,
            indexHtml({
                title: this.config.title ?? "",
                refHost: this.config.refHost,
                testHost: this.config.testHost,
                successItems,
                failedItems,
                successCount,
                failedCount
            })
        );
    }

    private copyAssets(baseDir: string, reportDir: string): void {
        console.log(`${path.dirname(require.main?.filename ?? "")}/assets`);
        DirsMaster.copyFolderRecursiveSync(
            `${path.dirname(require.main?.filename ?? "")}/assets`,
            `./${baseDir}/${reportDir}`
        );
    }
}
