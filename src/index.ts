#!/usr/bin/env node

import { HeadersConfig, TesterConfig } from "./core/interfaces/tester-config";
import { Tester } from "./core/tester";
import yargs from "yargs";
import { DirsMaster } from "./core/dirs-master";

function screenshotsTest(config: TesterConfig, headers?: HeadersConfig): void {
    const tester = new Tester(config, headers);
    tester
        .test()
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        .then(() => {})
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        .catch(() => {});
}

const packages = yargs
    .option("config", { type: "string", required: false })
    .option("title", { type: "string", required: false })
    .option("theme", { type: "array", required: false })
    .option("testHost", { type: "string", required: false })
    .option("refHost", { type: "string", required: false })
    .option("value-*", {
        type: "string",
        required: false,
        description:
            "You can pass any parameter instead of *. This will be available in the config as --value-*: value. Example: --value-password=123 -> you can use inputValue in your actions like --value-password"
    }).argv;

let config: TesterConfig | undefined;

const configPath = packages.config ?? ".ochkirc.js";
if (DirsMaster.exist(`${process.cwd()}/${configPath}`)) {
    try {
        // eslint-disable-next-line @typescript-eslint/no-require-imports,@typescript-eslint/no-var-requires
        config = require(`${process.cwd()}/${configPath}`) as TesterConfig;
    } catch (err) {
        console.error(`Can't find config ${process.cwd()}/${configPath}`);
    }
}
console.info(`Starting with config ${process.cwd()}/${configPath}`);

if (config) {
    if (packages.title && Object.prototype.hasOwnProperty.call(config, "title")) {
        config.title = packages.title;
    }
    if (packages.testHost && Object.prototype.hasOwnProperty.call(config, "testHost")) {
        config.testHost = packages.testHost;
    }
    if (packages.refHost && Object.prototype.hasOwnProperty.call(config, "refHost")) {
        config.refHost = packages.refHost;
    }
    if (packages.theme?.length && Object.prototype.hasOwnProperty.call(config, "themes")) {
        const themes = packages.theme.filter((theme) => config?.themes.includes(theme.toString()));
        if (themes.length) {
            config.themes = packages.theme as string[];
        }
    }

    const values = Object.keys(packages).filter((key) => key.startsWith("value-"));
    if (values.length) {
        const valuesObject: { [key: string]: string } = {};
        values.forEach((key) => {
            valuesObject[`--${key}`] = packages[key] as string;
        });
        config.values = valuesObject;
    }

    let headers: HeadersConfig | undefined = undefined;
    const cfClientId = Object.keys(packages).find((key) => key === "cf-access-client-id");
    const cfClientSecret = Object.keys(packages).find((key) => key === "cf-access-client-secret");
    if (cfClientId && cfClientSecret) {
        headers = {
            cfAccessClientId: packages["cf-access-client-id"] as string,
            cfAccessClientSecret: packages["cf-access-client-secret"] as string
        };
    }

    screenshotsTest(config, headers);
} else {
    console.error(`Add --config param or create .ochkirc.js in root folder`);
}
