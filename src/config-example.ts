import { TesterConfig } from "./core/interfaces/tester-config";

const configExample: TesterConfig = {
    dirs: {
        base: "./",
        report: "ochki-report",
        images: "images"
    },
    testHost: "https://google.com/",
    refHost: "https://google.com/",
    themes: ["default"],
    viewports: [
        {
            name: "mobile",
            width: 375,
            height: 10000, // see https://github.com/puppeteer/puppeteer/issues/2423
            userAgent:
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36"
        },
        {
            name: "tablet",
            width: 1340,
            height: 10000,
            userAgent:
                "Mozilla/5.0(iPad; U; CPU OS 4_3 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8F191 Safari/6533.18.5"
        },
        {
            name: "desktop",
            width: 1664,
            height: 10000,
            userAgent:
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36"
        }
    ],
    pages: [
        {
            name: "Index page",
            endpoint: "",
            queryParams: {
                mock: "true"
            },
            selectors: [
                {
                    viewport: ["tablet", "desktop", "mobile"],
                    selectors: [
                        {
                            name: "Body",
                            value: "#body"
                        }
                    ],
                    viewportHeight: 300
                }
            ]
        }
    ],
    misMatchPercentage: 0.05
};

module.exports = configExample;
